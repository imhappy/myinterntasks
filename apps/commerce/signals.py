from PIL import Image
from io import BytesIO
from django.dispatch import receiver
from apps.commerce.models import Product
from django.db.models.signals import pre_save
from django.core.files.uploadedfile import InMemoryUploadedFile


@receiver(pre_save, sender=Product)
def product_pre_save_handler(sender, instance, **kwargs):
    if instance.image:
        max_width = 700
        img = Image.open(instance.image)

        if img.width > max_width:
            aspect_ratio = img.width / img.height
            new_width = max_width
            new_height = int(new_width / aspect_ratio)
            img = img.resize((new_width, new_height), Image.LANCZOS)

        output = BytesIO()
        img.save(output, format="JPEG", quality=95)
        output.seek(0)

        instance.image = InMemoryUploadedFile(
            output,
            None,
            f"{instance.image.name.split('/')[-1]}",
            "image/jpeg",
            output.tell(),
            None,
        )
