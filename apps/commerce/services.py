from apps.commerce import models
from apps.common.services import global_all_objects


class ProductService:
    __product_model = models.Product

    @classmethod
    def get_or_none(cls, *args, **kwargs):
        try:
            cls.__product_model.objects.get(args, **kwargs)
        except:
            cls.__product_model.objects.none()

    @classmethod
    def get_all(cls, *args, **kwargs):
        return cls.__product_model.objects.all().select_related('category')

    @classmethod
    def get_filter(cls, **kwargs):
        return cls.__product_model.objects.filter(**kwargs)

    # Фильтрация только по пользователю который создал
    # @classmethod
    # def filter_objects_user(cls, user, *args, **kwargs):
    #     return cls.__product_model.objects.filter(user=user, *args, **kwargs)


class CategoryService:
    __category_model = models.Category

    @classmethod
    def get_or_none(cls, *args, **kwargs):
        try:
            cls.__category_model.objects.get(args, **kwargs)
        except:
            cls.__category_model.objects.none()

    @classmethod
    def get_all(cls):
        return cls.__category_model.objects.all()

    @classmethod
    def get_filter(cls, **kwargs):
        return cls.__category_model.objects.filter(**kwargs)

