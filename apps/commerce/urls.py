from django.urls import path
from apps.commerce import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register("categorys", views.CategoryAPIViewSet, basename='category')

urlpatterns = [
    path("products/", views.ProductListCreateAPIView.as_view(), name='product-list-create'),
    path("products/<int:pk>/", views.ProductRetrieveUpdateDestroyAPIView.as_view(), name='product-update-delete'),
]

urlpatterns += router.urls
