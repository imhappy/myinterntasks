# Generated by Django 4.2.4 on 2023-08-26 05:37

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("commerce", "0002_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="product",
            name="promotion",
            field=models.PositiveIntegerField(
                validators=[
                    django.core.validators.MinValueValidator(1),
                    django.core.validators.MaxValueValidator(100),
                ],
                verbose_name="акция%",
            ),
        ),
    ]
