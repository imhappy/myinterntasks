# Generated by Django 4.2.4 on 2023-08-28 04:46

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("commerce", "0003_alter_product_promotion"),
    ]

    operations = [
        migrations.AddField(
            model_name="category",
            name="turn_on",
            field=models.BooleanField(default=False),
        ),
    ]
