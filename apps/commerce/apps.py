from django.apps import AppConfig


class ProductsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.commerce"

    verbose_name = "Каталог"

    def ready(self):
        import apps.commerce.signals
