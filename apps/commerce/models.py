from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from apps.users.models import User


class Category(models.Model):
    title = models.CharField(max_length=50, unique=True, verbose_name="название")
    turn_on = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "commerce_category"
        verbose_name_plural = "Категории"
        verbose_name = "Категорию"


class Product(models.Model):
    title = models.CharField(max_length=50, unique=True, verbose_name="название")
    image = models.ImageField(upload_to="products/", blank=True, null=True, verbose_name="изображение")
    price = models.PositiveIntegerField(validators=[MinValueValidator(1)], verbose_name="цена", )
    in_stock = models.BooleanField(default=False, verbose_name="в наличии")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='создатель')
    promotion = models.PositiveIntegerField(validators=[MinValueValidator(1),
                                                        MaxValueValidator(100)], verbose_name="акция%")
    category = models.ForeignKey(to="Category", on_delete=models.CASCADE,
                                 related_name="product_category", verbose_name="категория")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="дата создания")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="дата изменения")

    def __str__(self):
        return self.title

    class Meta:
        db_table = "commerce_product"
        verbose_name_plural = "Продукты"
        verbose_name = "Продукт"
        ordering = ("-created_at",)
