from rest_framework import serializers
from apps.commerce.models import Product, Category
from apps.commerce import services


class CategoryModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ["title"]


class ProductModelSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=services.CategoryService.get_filter(turn_on=True))

    class Meta:
        model = Product
        exclude = ['created_by']

    def perform_create(self, serializer):
        serializer.save(created_by=self.context['request'].user)
