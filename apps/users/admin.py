from django.contrib import admin
from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ["id", "username", "email", "role", "is_active"]
    list_display_links = ["id", "username"]
    search_fields = ["username", "email"]
    ordering = ["-date_joined", "is_active", "is_staff"]
    actions_on_bottom = True
    list_editable = ["role"]
