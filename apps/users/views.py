from rest_framework import generics, permissions as ps
from apps.users import serializers, permissions, services


class UserListAPIView(generics.ListAPIView):
    queryset = services.UserService.get_all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]


class UserRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = services.UserService.get_all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]


class RegistrationAPIView(generics.CreateAPIView):
    serializer_class = serializers.RegistrationSerializer
    permission_classes = [ps.AllowAny]
