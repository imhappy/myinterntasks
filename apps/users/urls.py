from django.urls import path
from apps.users import views
from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenObtainPairView
)


urlpatterns = [
    path("users/", views.UserListAPIView.as_view()),
    path("users/<int:pk>/", views.UserRetrieveUpdateDestroyAPIView.as_view()),

    path("register/", views.RegistrationAPIView.as_view(), name="register"),
    path("login/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]

