from rest_framework import serializers
from apps.users import choices, services, models


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, min_length=4)

    class Meta:
        model = models.User
        fields = ['id', 'username', 'email', 'role', 'password']


class RegistrationSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=30, required=True)
    email = serializers.EmailField(required=True)
    password = serializers.CharField(write_only=True, min_length=4, required=True)
    role = serializers.ChoiceField(choices.ROLE_CHOICES, required=True)

    def create(self, validated_data):
        return services.UserService.create_user(**validated_data)

    def validate_email(self, value):
        if services.UserService.get_filter(email=value).exists():
            raise serializers.ValidationError('Такой email уже существует')
        return value
