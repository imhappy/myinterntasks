from apps.users.models import User


class UserService:
    __user_model = User

    @classmethod
    def get_or_none(cls, *args, **kwargs):
        try:
            cls.__user_model.objects.get(args, **kwargs)
        except:
            cls.__user_model.objects.none()

    @classmethod
    def get_all(cls):
        return cls.__user_model.objects.all()

    @classmethod
    def get_filter(cls, **kwargs):
        return cls.__user_model.objects.filter(**kwargs)

    @classmethod
    def create_user(cls, **kwargs):
        return cls.__user_model.objects.create_user(**kwargs)
