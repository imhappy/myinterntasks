from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    path("v1/commerce/", include("apps.commerce.urls")),
    path("v1/users/", include("apps.users.urls")),
]


urlpatterns += [path("i18n/", include("django.conf.urls.i18n"))]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
