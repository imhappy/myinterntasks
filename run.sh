#!/bin/bash

cd .
python manage.py makemigrations
python manage.py migrate

MANAGE_PY_PATH="./manage.py"

EMAIL="admin@mail.com"

USERNAME="admin"

PASSWORD="admin"

python "$MANAGE_PY_PATH" shell -c "
from apps.users.models import User
if not User.objects.filter(email='$EMAIL').exists():
    User.objects.create_superuser('$EMAIL', '$USERNAME', '$PASSWORD')
    print('Superuser created successfully')
else:
    print('Superuser already exists')"

python manage.py runserver
